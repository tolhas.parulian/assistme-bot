package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class InputEventReminderState extends State {
  public static final String DB_COL_NAME = "INPUT_REMINDER";

  public String chooseCategory(String type, String userId) {
    return "REMINDER";
  }

  public String inputCategory(String type, String userId) {
    return "REMINDER";
  }

  public String inputEventName(String name, String userId) {
    return "REMINDER";
  }

  public String inputEventTime(Date time, String userId) {
    return "REMINDER";
  }

  // public String inputEventReminder(Reminder time, String eventId){}

  /**
   * Undo to InputEventTimeState.
   * @param userId Intended user.
   * @return "UNDO"
   */
  public String undoInputEvent(String userId) {
    LineUser user = lineUserRepository.findLineUserByUserId(userId);

    user.setState(InputEventTimeState.DB_COL_NAME);
    lineUserRepository.save(user);
    return "UNDO";
  }

  public String deleteEvent(String eventId, String userId) {
    return "REMINDER";
  }

  @Override
  public String toString() {
    return "InputEventReminderState";
  }

  /**
   * Report mood.
   * @return list of response message
   */
  public List<Message> reportMood() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Process mood.
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Other command is accessed by user.
   * @return list of response message
   */
  public List<Message> others() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.OTHERS_TEXT);
    return response;
  }

  /**
   * Help command is accessed by user.
   * @return list of response message
   */
  public List<Message> help() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    return response;
  }

  /**
   * Get an event.
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Get all events based on userId.
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Get some events based on userId & event category.
   * @param category event category
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

}