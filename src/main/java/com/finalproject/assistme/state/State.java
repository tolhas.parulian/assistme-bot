package com.finalproject.assistme.state;

import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class State {
  @Autowired
  LineMessagingClient lineMessagingClient;

  @Autowired
  LineUserRepository lineUserRepository;

  @Autowired
  EventRepository eventRepository;
    
  public abstract String chooseCategory(String type, String userId);

  public abstract String inputCategory(String type, String userId);

  public abstract String inputEventName(String name, String userId);

  public abstract String inputEventTime(Date time, String userId);

  // public abstract String inputEventReminder(Date time, Long eventId);

  public abstract String undoInputEvent(String userId);

  public abstract String deleteEvent(String eventId, String userId);

  public abstract List<Message> reportMood();

  public abstract List<Message> processMood(String moodType);

  public abstract List<Message> others();

  public abstract List<Message> help();

  public abstract List<Message> getEvent(String eventId, String userId, String displayName);

  public abstract List<Message> getAllEvents(String userId, String displayName);

  public abstract List<Message> getEventsByCategory(String category,
                                                    String userId,
                                                    String displayName);

}