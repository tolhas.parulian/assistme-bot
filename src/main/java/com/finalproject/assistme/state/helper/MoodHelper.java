package com.finalproject.assistme.state.helper;

import com.finalproject.assistme.core.flexmessage.PlaylistButton;
import com.finalproject.assistme.core.mood.Mood;
import com.finalproject.assistme.core.mood.MoodFactory;
import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.Present;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MoodHelper {

  private MoodFactory factory;

  public MoodHelper() {
    this.factory = new MoodFactory();
  }

  /**
   * Helper for processing mood.
   * @param moodType type of the mood
   * @return list of response messages
   */
  public List<Message> processMood(String moodType) {

    Mood mood = factory.createMood(moodType);
    Present present = mood.process();
    List<Message> messageListResponse = new ArrayList<>(Arrays.asList(null, null, null));
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    CompletableFuture<Void> processImage = CompletableFuture.runAsync(() -> {
      try {
        Image presentImage = present.getImage();
        String imageUrl = presentImage.getImage();
        ImageMessage imageMessage = new ImageMessage(imageUrl, imageUrl);
        messageListResponse.set(0, imageMessage);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }, executorService);

    CompletableFuture<Void> processQuotes = CompletableFuture.runAsync(() -> {
      try {
        Quote presentQuote = present.getQuote();
        String message = presentQuote.getQuote();
        TextMessage quoteMessage = new TextMessage(message);
        messageListResponse.set(1, quoteMessage);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }, executorService);

    CompletableFuture<Void> processButton = CompletableFuture.runAsync(() -> {
      try {
        PlaylistButton buttonMessage = new PlaylistButton();
        messageListResponse.set(2, buttonMessage.get(moodType));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }, executorService);

    CompletableFuture<Void> combinedProcess = CompletableFuture
        .allOf(processImage, processQuotes, processButton);
    combinedProcess.join();
    
    return messageListResponse;
  }
}