package com.finalproject.assistme.state;

import com.finalproject.assistme.core.flexmessage.EventCard;
import com.finalproject.assistme.core.flexmessage.EventListCard;
import com.finalproject.assistme.core.flexmessage.MoodReplyMessage;
import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.state.helper.MoodHelper;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.stereotype.Component;

@Component
public class IdleState extends State {
  public static final String DB_COL_NAME = "IDLE";

  public String chooseCategory(String type, String userId) {
    return "IDLE";
  }

  public String inputCategory(String type, String userId) {
    return "IDLE";
  }

  /**
   * Input name for event.
   * @param name Event name.
   * @param userId User id.
   * @return "SUCCESS"
   */
  public String inputEventName(String name, String userId) {
    Event event = eventRepository.findUnverifiedEventByUserId(userId);

    event.setName(name);

    LineUser user = event.getUser();
    user.setState(InputCategoryState.DB_COL_NAME);

    eventRepository.save(event);
    lineUserRepository.save(user);
    return "SUCCESS";
  }

  public String inputEventTime(Date time, String userId) {
    return "IDLE";
  }

  // public String inputEventReminder(Date time, String eventId){}

  public String undoInputEvent(String userId) {
    return "IDLE";
  }

  /**
   * Delete event.
   * @param eventId Event that want to be deleted.
   * @return "SUCCESS"
   */
  public String deleteEvent(String eventId, String userId) {
    eventRepository.deleteEventByEventId(Long.parseLong(eventId), userId);
    return "SUCCESS";
  }

  @Override
  public String toString() {
    return "IdleState";
  }

  /**
   * Report mood.
   * @return list of response message
   */
  public List<Message> reportMood() {
    List<Message> response = new ArrayList<>();
    response.add(new MoodReplyMessage().getMoodReplyMessage());
    return response;
  }

  /**
   * Process mood.
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    MoodHelper moodHelper = new MoodHelper();
    List<Message> response = moodHelper.processMood(moodType);
    return response;
  }

  /**
   * Other command is accessed by user.
   * @return list of response message
   */
  public List<Message> others() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.OTHERS_TEXT);
    return response;
  }

  /**
   * Help command is accessed by user.
   * @return list of response message
   */
  public List<Message> help() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    return response;
  }

  /**
   * Get an event.
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    try {
      Event event = eventRepository.findEventByEventId(Long.parseLong(eventId),
              userId);
      if (event == null) {
        response.add(Messages.EVENT_NOT_FOUND);
        return response;
      } else {
        FlexMessage flexGetEvent = new EventCard().get(event, displayName);
        response.add(flexGetEvent);
        return response;
      }
    } catch (Exception e) {
      response.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return response;
    }
  }

  /**
   * Get all events based on userId.
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    try {
      CompletableFuture<List<Event>> lineUserAsync = eventRepository
              .findEventsByUserIdAsync(userId);
      List<Event> listEvent = lineUserAsync.get();
      if (listEvent.isEmpty()) {
        response.add(Messages.EVENT_NOT_FOUND);
        return response;
      } else {
        FlexMessage flexMessageResponse = new EventListCard().get(
                listEvent, true, "general", displayName);
        response.add(flexMessageResponse);
        return response;
      }
    } catch (Exception e) {
      response.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return response;
    }
  }

  /**
   * Get some events based on userId & event category.
   * @param category event category
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    try {
      List<Event> listEventC = eventRepository.findEventsByCategory(category, userId);
      if (listEventC.isEmpty()) {
        response.add(Messages.NO_EVENT_WITH_PROVIDED_CATEGORY);
        return response;
      } else {
        FlexMessage flexMessageEC = new EventListCard().get(
                listEventC, false, category, displayName);
        response.add(flexMessageEC);
        return response;
      }
    } catch (Exception e) {
      response.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return response;
    }
  }

}