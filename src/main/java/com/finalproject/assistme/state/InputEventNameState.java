package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class InputEventNameState extends State {
  public static final String DB_COL_NAME = "INPUT_NAME";

  public String chooseCategory(String type, String userId) {
    return "NAME";
  }

  public String inputCategory(String type, String userId) {
    return "NAME";
  }

  /**
   * Input name for event.
   * @param name Event name.
   * @param userId User id.
   * @return "SUCCESS"
   */
  public String inputEventName(String name, String userId) {
    Event event = eventRepository.findUnverifiedEventByUserId(userId);

    event.setName(name);
    
    LineUser user = event.getUser();
    user.setState(InputCategoryState.DB_COL_NAME);
    
    eventRepository.save(event);
    lineUserRepository.save(user);
    return "SUCCESS";
  }

  public String inputEventTime(Date time, String userId) {
    return "NAME";
  }

  // public String inputEventReminder(Date time, String eventId){}

  public String undoInputEvent(String userId) {
    return "UNDO";
  }

  public String deleteEvent(String eventId, String userId) {
    return "NAME";
  }

  @Override
  public String toString() {
    return "InputEventNameState";
  }

  /**
   * Report mood.
   * @return list of response message
   */
  public List<Message> reportMood() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Process mood.
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Other command is accessed by user.
   * @return list of response message
   */
  public List<Message> others() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.OTHERS_TEXT);
    return response;
  }

  /**
   * Help command is accessed by user.
   * @return list of response message
   */
  public List<Message> help() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    return response;
  }

  /**
   * Get an event.
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Get all events based on userId.
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

  /**
   * Get some events based on userId & event category.
   * @param category event category
   * @param userId user id of user that own the event
   * @param displayName user display name
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId, String displayName) {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    return response;
  }

}