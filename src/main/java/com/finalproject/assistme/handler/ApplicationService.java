package com.finalproject.assistme.handler;

import com.linecorp.bot.model.message.Message;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ApplicationService implements Service {

  private Handler handler;

  public ApplicationService() {
  }

  public ApplicationService(Handler handler) {
    this.handler = handler;
  }

  @Override
  public List<Message> getResponse() {
    return this.handler.getResponse();
  }

  @Override
  public void setHandler(Handler handler) {
    this.handler = handler;
  }
    
}