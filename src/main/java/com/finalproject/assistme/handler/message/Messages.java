package com.finalproject.assistme.handler.message;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class Messages {

  public static final Message HELP_TEXT = new TextMessage(
      "List of available commands \n"
      + "/help - Show all valid commands \n"
      + "/reportMood - Report your mood \n"
      + "/createEvent [event name] - Make your Event \n"
      + "/getAllEvents - List all your events \n"
      + "/getEventsbyCategory [category name] - List events sorted by category \n"
  );

  public static final Message OTHERS_TEXT = new TextMessage(
      "Invalid command! Please type '/help' to show all valid commands"
  );

  public static final Message INVALID_COMMAND_AT_WRONG_STATE = new TextMessage(
      "Invalid command! You're not in the right state to do this!"
  );

  public static final Message EVENT_NOT_FOUND = new TextMessage(
          "No event found"
  );

  public static final Message NO_EVENT_WITH_PROVIDED_CATEGORY = new TextMessage(
          "There's no event with provided category"
  );

  public static final Message ERROR_WHILE_FETCHING_EVENT = new TextMessage(
          "Error fetching event(s)"
  );

}