package com.finalproject.assistme.controller;

import com.finalproject.assistme.core.flexmessage.CategoryReplyMessage;
import com.finalproject.assistme.core.flexmessage.EventCard;
import com.finalproject.assistme.core.flexmessage.EventListCard;
import com.finalproject.assistme.core.flexmessage.MoodReplyMessage;
import com.finalproject.assistme.core.flexmessage.PlaylistButton;
import com.finalproject.assistme.core.flexmessage.PlaylistMessage;
import com.finalproject.assistme.core.flexmessage.TrackReplyMessage;
import com.finalproject.assistme.core.mood.Mood;
import com.finalproject.assistme.core.mood.MoodFactory;
import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.Present;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import com.finalproject.assistme.handler.ApplicationService;
import com.finalproject.assistme.handler.UniversalHandler;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.musicplayer.Spotify;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.InputEventNameState;
import com.finalproject.assistme.state.State;
import com.finalproject.assistme.state.helper.StateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.AudioMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;
import java.lang.Long;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class AssistMeController {

  // private MoodFactory factory = new MoodFactory();

  @Autowired
  private LineMessagingClient lineMessagingClient;

  @Autowired
  private LineUserRepository lineUserRepository;

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private StateHelper stateHelper;

  @Autowired
  private ApplicationService applicationService;

  @Autowired
  private UniversalHandler universalHandler;

  public AssistMeController() {
    Spotify.clientCredentials_Sync();
  }

  /**
   * Text Message Handler.
   *
   * @param event represent message event that occurs.
   */
  @EventMapping
  public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
    String replyToken = event.getReplyToken();
    final String originalMessageText = event.getMessage().getText();
    String[] splittedMessage = originalMessageText.split(" ");
    String command = splittedMessage[0];
    Source source = event.getSource();
    String userId = source.getUserId();
    String displayName = getUserDisplayName(userId);

    if (isLineUserRegistered(userId)) {
      this.processCommand(command,
              replyToken,
              displayName,
              splittedMessage,
              userId,
              originalMessageText);
    } else if (command.equals("/register")) {
      String userName;
      if (splittedMessage.length == 1) {
        userName = "";
      } else {
        userName = splittedMessage[1];
      }
      String responseText;
      if (userName.equals("") || userId.equals("") || displayName.equals("")) {
        responseText = "username/id/displayName invalid";
      } else {
        LineUser newUser = new LineUser(userId, userName, displayName);
        lineUserRepository.save(newUser);
        responseText = "user berhasil dibuat";
      }
      TextMessage response = new TextMessage(responseText);
      reply(replyToken, response);
    } else {
      TextMessage response = new TextMessage("kamu belum teregister, ketik /register [nama]");
      reply(replyToken, response);
    }
  }

  /**
   * Get user display.
   *
   * @param command represent command from user.
   * @param replyToken represent replyToken.
   * @param displayName represent displayName from line user.
   * @param splittedMessage represent list from splitted input message.
   * @param userId represent userId from user.
   * @param originalMessageText represent original text.
   */
  public void processCommand(String command,
                             String replyToken,
                             String displayName,
                             String[] splittedMessage,
                             String userId,
                             String originalMessageText
  ) {
    State state = stateHelper.getUserState(userId);
    LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
    switch (command) {

      case "/music":
        String trackId = splittedMessage[1];
        AudioMessage audioMessageResponse = new AudioMessage(Spotify.getTrackSync(trackId), 30000);
        reply(replyToken, audioMessageResponse);
        break;

      case "/mood":
        String moodType = splittedMessage[1];
        universalHandler.setResponse(
                state.processMood(moodType)
        );
        applicationService.setHandler(universalHandler);
        List<Message> responseMessage = applicationService.getResponse();
        reply(replyToken, responseMessage);
        break;

      case "/playlist":
        String query = splittedMessage[1];
        SpotifyPlaylist playLists = new SpotifyPlaylist("dummy");
        PlaylistSimplified playList = playLists.getPlaylists(query);
        com.wrapper.spotify.model_objects.specification.Image img = null;
        for (com.wrapper.spotify.model_objects.specification.Image image : playList.getImages()) {
          img = image;
          break;
        }
        System.out.println(playList.getExternalUrls().getExternalUrls().get("spotify"));

        FlexMessage response = new PlaylistMessage()
                .get(img.getUrl(), playList.getExternalUrls().get("spotify"), playList.getName(),
                        playList.getName() + " Playlist", "/showTracks " + playList.getId());
        reply(replyToken, response);
        break;

      case "/showTracks":
        // Todo : implement fitur show track-nya
        String playListId = splittedMessage[1];
        playLists = new SpotifyPlaylist("dummy");
        ArrayList<Track> tracks = playLists.getTracks(playListId);
        Message trackReplyMessageResponse = new TrackReplyMessage().get(tracks);
        reply(replyToken, trackReplyMessageResponse);
        break;

      case "/reportMood":
        universalHandler.setResponse(
                state.reportMood()
        );
        applicationService.setHandler(universalHandler);
        responseMessage = applicationService.getResponse();
        reply(replyToken, responseMessage);
        break;

      case "/createEvent":
        List<Message> messageEventListResponse = new ArrayList<>();
        if (splittedMessage.length == 1) {
          messageEventListResponse.add(
                  new TextMessage("Wrong input\ne.g '/createEvent [event name]'"));
        } else {
          if (eventRepository.findUnverifiedEventByUserId(userId) == null) {
            String name = this.getEventName(subArray(splittedMessage, 1,
                    splittedMessage.length));
            Event event = new Event(lineUser, "placeholder", "placeholder", new Date());
            eventRepository.save(event);
            state.inputEventName(name, userId);
            messageEventListResponse.add(new TextMessage("Event name done"));
            messageEventListResponse.add(new CategoryReplyMessage().getCategoryReplyMessage());
          } else {
            Event notDone = eventRepository.findUnverifiedEventByUserId(userId);
            messageEventListResponse.add(new TextMessage("You have undone event with ID: "
                    + notDone.getEventId()));
            messageEventListResponse.add(
                    new TextMessage("Finish that first or /deleteEvent [ID] to continue"));
          }
        }
        reply(replyToken, messageEventListResponse);
        break;

      case "/choose":
        List<Message> messageChooseResponse = new ArrayList<>();
        if (splittedMessage.length == 1) {
          messageChooseResponse.add(
                  new TextMessage("Format\n/choose [kategori]"));
        } else {
          String categoryNumber = splittedMessage[1];
          try {
            if (categoryNumber.equals("1") || categoryNumber.equals("2")
                    || categoryNumber.equals("3")) {
              state.chooseCategory(categoryNumber, userId);
            } else {
              state.inputCategory(categoryNumber, userId);
            }
            messageChooseResponse.add(
                    new TextMessage("Kategori berhasil ditambah"));
            messageChooseResponse.add(
                    new TextMessage("Ketik /date untuk menambahkan tanggal\n"
                            +  "e.g: /date 2020-01-01 (YYYY-MM-DD"));
          } catch (NullPointerException e) {
            messageChooseResponse.add(
                    new TextMessage("There's no draft events"));
          }
        }
        reply(replyToken, messageChooseResponse);
        break;

      case "/date":
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        String responseTextDate;

        if (splittedMessage.length == 1) {
          responseTextDate = "Wrong input\nInput date using YYYY-MM-DD format";
        } else {
          String dateString = splittedMessage[1];
          Date date = new Date();
          try {
            date = dateFormatter.parse(dateString);
            if (state.inputEventTime(date, userId).equals("SUCCESS")) {
              responseTextDate = "Tanggal event berhasil ditambah\nEvent done!!! "
                      + "(Reminder TBA [shella])";
            } else {
              responseTextDate = "Trying to go back to the past?";
            }
          } catch (ParseException e) {
            responseTextDate = "Wrong input\nInput date using YYYY-MM-DD format";
          } catch (NullPointerException e) {
            responseTextDate = "There's no draft event";
          }
        }
        TextMessage responseDateMessage = new TextMessage(responseTextDate);
        reply(replyToken, responseDateMessage);
        break;

      case "/deleteEvent":
        String responseDelete;
        if (splittedMessage.length == 1) {
          responseDelete = "Tambahkan event yang ingin di hapus\ne.g /deleteEvent [idEvent]";
        } else {
          try {
            String namaEvent = eventRepository.findEventByEventId(
                    Long.parseLong(splittedMessage[1]), userId).getName();
            state.deleteEvent(splittedMessage[1], userId);
            responseDelete = "Event " + namaEvent + " berhasil di hapus";
          } catch (NullPointerException e) {
            responseDelete = "Event not found";
          }
        }
        Message responseDeleteMsg = new TextMessage(responseDelete);
        reply(replyToken, responseDeleteMsg);
        break;

      case "/undoInput":
        if (lineUser.getState().equals("IDLE")) {
          reply(replyToken, new TextMessage("Nothing to undo"));
        } else {
          try {
            String responseUndo;
            state.undoInputEvent(userId);
            String now = lineUserRepository.findStateByUserId(userId);
            responseUndo = "Undo to " + now;
            reply(replyToken, new TextMessage(responseUndo));
          } catch (NullPointerException e) {
            reply(replyToken, new TextMessage("Nothing to undo"));
          }
        }
        break;

      case "/getEvent":
        if (splittedMessage.length == 1) {
          reply(replyToken, new TextMessage("Format\n/getEvent [event ID]"));
        } else {
          universalHandler.setResponse(
                  state.getEvent(splittedMessage[1], userId, displayName)
          );
          applicationService.setHandler(universalHandler);
          responseMessage = applicationService.getResponse();
          reply(replyToken, responseMessage);
        }
        break;

      case "/getAllEvents":
        universalHandler.setResponse(
                state.getAllEvents(userId, displayName)
        );
        applicationService.setHandler(universalHandler);
        responseMessage = applicationService.getResponse();
        reply(replyToken, responseMessage);
        break;

      case "/getEventsbyCategory":
        String responseEventC;
        if (splittedMessage.length == 1) {
          responseEventC = "Format /getEventsbyCategory [category name]";
          reply(replyToken, new TextMessage(responseEventC));
        } else {
          universalHandler.setResponse(
                  state.getEventsByCategory(splittedMessage[1], userId, displayName)
          );
          applicationService.setHandler(universalHandler);
          responseMessage = applicationService.getResponse();
          reply(replyToken, responseMessage);
        }
        break;

      case "/testFlex":
        LineUser dummyLineUser1 = new LineUser("userId","userName","displayName");
        Event event11 = new Event(dummyLineUser1,"description1","category1",new Date());
        FlexMessage flexMessageResponse1 = new EventCard().get(event11, "displayName");
        reply(replyToken, (Message)flexMessageResponse1);
        break;

      case "/testFlexList":
        LineUser dummyLineUser = new LineUser("userId","userName","displayName");
        Event event1 = new Event(dummyLineUser,"description1","category1",new Date());
        Event event2 = new Event(dummyLineUser,"description2","category2",new Date());
        List<Event> eventList = new ArrayList<>();
        eventList.add(event1);
        eventList.add(event2);
        FlexMessage flexMessageResponse = new EventListCard().get(eventList, true,
            "general", "userDisplayName");
        reply(replyToken, (Message)flexMessageResponse);
        break;
      case "/help":
        universalHandler.setResponse(state.help());
        applicationService.setHandler(universalHandler);
        responseMessage = applicationService.getResponse();
        reply(replyToken, responseMessage);
        break;

      default:
        universalHandler.setResponse(state.others());
        applicationService.setHandler(universalHandler);
        responseMessage = applicationService.getResponse();
        reply(replyToken, responseMessage);
        break;
    }
  }

  private void reply(String replyToken, Message message) {
    try {
      lineMessagingClient.replyMessage(
              new ReplyMessage(replyToken, message)).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    } catch (NullPointerException e) {
      System.out.println(e);
    }
  }

  private void reply(String replyToken, List<Message> message) {
    try {
      lineMessagingClient.replyMessage(
              new ReplyMessage(replyToken, message)).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    } catch (NullPointerException e) {
      System.out.println(e);
    }
  }

  /**
   * Get user display.
   *
   * @param userId represent userId from lineUser.
   */
  public String getUserDisplayName(String userId) {
    try {
      return lineMessagingClient.getProfile(userId).get().getDisplayName();
    } catch (ExecutionException | InterruptedException e) {
      return e.getMessage();
    } catch (NullPointerException e) {
      return e.getMessage();
    }
  }

  /**
   * Get user display.
   *
   * @param userId represent userId from lineUser.
   */
  public boolean isLineUserRegistered(String userId) {
    if (lineUserRepository.isLineUserRegistered(userId)) {
      return true;
    } else {
      return false;
    }
  }

  public String getEventName(String[] arrMessage) {
    String joinedMessage = String.join(" ", arrMessage);
    return joinedMessage;
  }

  //references: https://www.techiedelight.com/get-subarray-array-specified-indexes-java/
  public <T> T[] subArray(T[] array, int start, int end) {
    return Arrays.copyOfRange(array, start, end);
  }


}