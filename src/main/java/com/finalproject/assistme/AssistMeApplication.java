package com.finalproject.assistme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class AssistMeApplication {
  public static void main(String[] args) {
    SpringApplication.run(AssistMeApplication.class, args);
  }
}
