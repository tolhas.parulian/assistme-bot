package com.finalproject.assistme.musicplayer;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsTracksRequest;
import com.wrapper.spotify.requests.data.search.simplified.SearchPlaylistsRequest;
import com.wrapper.spotify.requests.data.tracks.GetTrackRequest;
import java.io.IOException;
import java.util.ArrayList;

public class Spotify {

  private static final String clientId = "28145851ec444ef088584a4233382680";
  private static final String clientSecret = "100a73598b9345e69800bb50a33321b2";

  private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
      .setClientId(clientId)
      .setClientSecret(clientSecret)
      .build();
  private static final ClientCredentialsRequest clientCredentialsRequest = spotifyApi
      .clientCredentials()
      .build();

  /**
   * Method to get Track from spotify.
   * @param trackId unique id for spotify track
   * @return preview url to show
   */
  public static String getTrackSync(String trackId) {
    try {
      final GetTrackRequest getTrackRequest = spotifyApi.getTrack(trackId)
          .build();
      final Track track = getTrackRequest.execute();
      return track.getPreviewUrl();
    } catch (IOException | SpotifyWebApiException e) {
      return "Error: " + e.getMessage();
    }
  }

  /**
   * Method to get list of tracks for given id.
   * @param playlistId unique id for playlist id
   * @return
   */
  public static ArrayList<Track> getPlaylistsTracksSync(String playlistId) {
    try {
      final GetPlaylistsTracksRequest getPlaylistsTracksRequest = spotifyApi
          .getPlaylistsTracks(playlistId)
          .build();
      final Paging<PlaylistTrack> playlistTrackPaging = getPlaylistsTracksRequest.execute();
      ArrayList<Track> tracks = new ArrayList<>();
      for (PlaylistTrack playlistTrack : playlistTrackPaging.getItems()) {
        tracks.add(playlistTrack.getTrack());
        if (tracks.size() == 5) {
          break;
        }
      }
      return tracks;

    } catch (IOException | SpotifyWebApiException e) {
      System.out.println("Error: " + e.getMessage());
      return null;
    }
  }

  /**
   * Method to get playlist information for given Id.
   * @param query keyword used for search
   * @return
   */
  public static ArrayList<PlaylistSimplified> searchPlayList(String query) {
    SearchPlaylistsRequest searchPlaylistsRequest = spotifyApi.searchPlaylists(query)
        .limit(1)
        .build();

    ArrayList<PlaylistSimplified> listId = new ArrayList<PlaylistSimplified>();
    try {
      for (PlaylistSimplified item : searchPlaylistsRequest.execute().getItems()) {
        listId.add(item);
      }
      return listId;
    } catch (IOException | SpotifyWebApiException e) {
      return null;
    }
  }

  /**
   * Method to set access API token.
   * @return
   */
  public static void clientCredentials_Sync() {
    try {
      final ClientCredentials clientCredentials = clientCredentialsRequest.execute();

      // Set access token for further "spotifyApi" object usage
      spotifyApi.setAccessToken(clientCredentials.getAccessToken());
      System.out.println("Expires in: " + clientCredentials.getExpiresIn());
    } catch (IOException | SpotifyWebApiException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }


}