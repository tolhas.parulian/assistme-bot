package com.finalproject.assistme.core.present;

public class ImageUrlList {
    
  public static String[] happyList = 
      {"https://c.stocksy.com/a/xeY000/z9/133235.jpg",
      "https://graphicriver.img.customer.envatousercontent.com/files/70040242/13_sun_001.jpg?auto=compress%2Cformat&q=80&fit=crop&crop=top&max-h=8000&max-w=590&s=9d3ff78a6eb5246fc6d0a1fadfaca4f1",
      "https://thumbs.dreamstime.com/z/cheerful-emoticon-21967172.jpg",
      "https://i.pinimg.com/564x/2d/cb/88/2dcb88c28ab92047c7a884199a2c990e.jpg",
      "https://i.pinimg.com/564x/0c/cd/7d/0ccd7ddcda2a132c2464af071581707f.jpg"};
  public static String[] sadList = 
      {"https://i.pinimg.com/564x/87/99/60/879960c37016c4e35c5b8224e0391028.jpg",
      "https://i.pinimg.com/564x/62/d3/9d/62d39da6400012322683ac7b1c38ec85.jpg",
      "https://i.pinimg.com/236x/07/bd/5c/07bd5c46d04b78b0125c32a0428c16f0.jpg",
      "https://i.pinimg.com/564x/71/cb/2c/71cb2ccbe1d820b0556e0b2e0cbc90cc.jpg",
      "https://i.pinimg.com/564x/7b/31/0d/7b310d2b97e50a1e4d1145e8ce4c9b08.jpg"};
  public static String[] otherList = 
      {"https://i.pinimg.com/564x/df/0e/6b/df0e6b9ed3ffff4a2e221ba19766414b.jpg",
      "https://i.pinimg.com/564x/79/ce/20/79ce20ddc77b067a300c810e8cb8e515.jpg",
      "https://i.pinimg.com/564x/84/0e/63/840e63e620fed089d864cee50f868bf2.jpg",
      "https://i.pinimg.com/564x/29/dd/9d/29dd9dd740d114a37f23cf1a04a814f7.jpg",
      "https://i.pinimg.com/564x/6b/ba/64/6bba6457ac712f0fd7ea52d77111d683.jpg"};
  public static String[] tiredList = 
      {"https://i.pinimg.com/564x/ab/81/a4/ab81a4057468736e7957bae748bbbf6d.jpg", 
      "https://i.pinimg.com/564x/29/96/22/29962286c243346f492f956426be39cd.jpg", 
      "https://i.pinimg.com/564x/cf/7a/ca/cf7acac7fa1c57d4deabfa0246a923b5.jpg",
      "https://i.pinimg.com/564x/f2/2b/5f/f22b5fae6860c5b87bc73f013e0f42f8.jpg", 
      "https://i.pinimg.com/564x/97/9a/37/979a37cd9dd922a9f65ca2656242adc9.jpg"};
  public static String[] productiveList = 
      {"https://i.pinimg.com/564x/72/ec/8b/72ec8bd77f866b9b8a8d7af3df73ac42.jpg",
      "https://i.pinimg.com/564x/e8/b1/3e/e8b13e3109dd50c24775b988514ae836.jpg",
      "https://i.pinimg.com/564x/ac/2e/05/ac2e055ef247a653dc543cb3d36a8aed.jpg",
      "https://i.pinimg.com/564x/63/72/c8/6372c8bc64463f2fdaa63b817afd4d24.jpg",
      "https://i.pinimg.com/564x/52/d2/27/52d227439907b01a0b7d5964220b2cd4.jpg"};
}
