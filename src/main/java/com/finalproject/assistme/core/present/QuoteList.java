package com.finalproject.assistme.core.present;

public class QuoteList {

  public static String[] happyList = 
      {"\"Your presence can give happiness. I hope you remember that.\"-ksj",
      "\"Go on your path, even if you live for a day.\"-pjm",
      "\"Just live how you want, your life is yours. Don't try so hard, it's okay to lose\"-anon",
      "\"I have not failed. I've just found 10000 ways that wont work.\"-thomas alva edison",
      "\"Life is what happens to us while we are making other plans.\"-allen saunders"};
  public static String[] sadList = 
      {"\"Don't dissapear because you're a big existence.\"-anon",
      "\"Forget the past because everything is new at zero o'clock.\"-anon",
      "\"This too shall pass.\"-anon",
      "\"You're too young to let the world break you.\"-kth",
      "\"You will fully bloom after all the hardships.\"-myg"};
  public static String[] otherList = 
      {"\"I'm the one I should love, in this world.\"-anon",
      "\"The me of yesterday, today, and tomorrow, with no exceptions, it's all me.\"-anon",
      "\"Follow your dream like breaker, even if it breaks down, don't ever run backwards.\"-anon",
      "\"Don't be captured in someone else's dream.\"-anon",
      "\"One history in one person. One star in one person.\"-anon"};
  public static String[] tiredList = 
      {"\"Don't push yourself too hard. Don't forget to take a rest!\"-anon",
      "\"You never walk alone.\"-anon",
      "\"Shine, dream, smile.\"-anon",
      "\"Everyone's going to hurt you. You just got to find the ones worth suffering for.\"-bob m",
      "\"The deeper the night, the brighter the starlight.\"-anon"};
  public static String[] productiveList =
      {"\"You're really the one who needs to acknowledge your effort.\"-ksj",
      "\"You will regret someday if you don't do your best now.\"-jjk",
      "\"Yes it is going to be hard, but it is going to be worth it.\"-anon",
      "\"It's the path you chose. Don't be scared. This is just the first flight!\"-anon",
      "\"Teamwork makes the dream work.\"-knj"
      };
  
}
