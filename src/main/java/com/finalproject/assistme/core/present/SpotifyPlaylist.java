package com.finalproject.assistme.core.present;

import com.finalproject.assistme.musicplayer.Spotify;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;
import java.util.ArrayList;

public class SpotifyPlaylist {

  private String message;

  public SpotifyPlaylist(String message) {
    this.message = message;
  }

  public String getSpotifyPlaylist() {
    return this.message;
  }

  /**
   * Return Playlist for given search query.
   * @param query keyword to search playlist
   * @return Playlist information that is looking for
   */
  public PlaylistSimplified getPlaylists(String query) {
    ArrayList<PlaylistSimplified> playlistSimplified;
    playlistSimplified = Spotify.searchPlayList(query);
    PlaylistSimplified item = playlistSimplified.get(0);
    return item;
  }

  public ArrayList<Track> getTracks(String playlistId) {
    ArrayList<Track> tracks = Spotify.getPlaylistsTracksSync(playlistId);
    return tracks;
  }

}
