package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

public class PlaylistButton {
  /**
   * Get message to show playlist and its description.
   * @param command command used for show playlist music
   * @return FlexMessage
   */
  public FlexMessage get(String command) throws InterruptedException  {
    final Box bodyBlock = createBodyBlock(command);
    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .build();

    return new FlexMessage("assistme-Bot sent you a playlist button", bubble);
  }

  private Box createBodyBlock(String command) {
    final Button websiteAction =
        Button.builder()
            .style(ButtonStyle.LINK)
            .height(ButtonHeight.SMALL)
            .action(new MessageAction("Show playlist", "/playlist " + command))
            .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .spacing(FlexMarginSize.SM)
        .contents(asList(websiteAction))
        .build();
  }



}
