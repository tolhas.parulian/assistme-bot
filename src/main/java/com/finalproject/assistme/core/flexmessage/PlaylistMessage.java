package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectMode;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectRatio;
import com.linecorp.bot.model.message.flex.component.Image.ImageSize;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Spacer;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.net.URI;

public class PlaylistMessage {

  /**
   * Get message to show playlist and its description.
   * @param imgUrl image url for playlist
   * @param uriPlaylist refer to playlist application
   * @param name playlist name
   * @param description additional description
   * @param command command used for show track music
   * @return FlexMessage
   */
  public FlexMessage get(String imgUrl, String uriPlaylist, String name, String description,
      String command) {
    final Image heroBlock =
        Image.builder()
            .url(URI.create(imgUrl).toString())
            .size(ImageSize.FULL_WIDTH)
            .aspectRatio(ImageAspectRatio.R20TO13)
            .aspectMode(ImageAspectMode.Cover)
            .build();

    final Box bodyBlock = createBodyBlock(name, description);
    final Box footerBlock = createFooterBlock(uriPlaylist, command);
    final Bubble bubble =
        Bubble.builder()
            .hero(heroBlock)
            .body(bodyBlock)
            .footer(footerBlock)
            .build();

    return new FlexMessage("assistme-Bot sent you a playlist", bubble);
  }

  private Box createFooterBlock(String uriPlayList, String command) {
    final Spacer spacer = Spacer.builder().size(FlexMarginSize.SM).build();
    final Button callAction = Button
        .builder()
        .style(ButtonStyle.LINK)
        .height(ButtonHeight.SMALL)
        .action(new URIAction("Listen on Spotify", URI.create(uriPlayList).toString()))
        .build();
    final Separator separator = Separator.builder().build();
    final Button websiteAction =
        Button.builder()
            .style(ButtonStyle.LINK)
            .height(ButtonHeight.SMALL)
            .action(new MessageAction("Show Tracks(max: 5)", command))
            .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .spacing(FlexMarginSize.SM)
        .contents(asList(spacer, callAction, separator, websiteAction))
        .build();
  }

  private Box createBodyBlock(String name, String description) {
    final Text title =
        Text.builder()
            .text(name)
            .weight(TextWeight.BOLD)
            .size(FlexFontSize.XL)
            .build();

    final Box info = createInfoBox(description);

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(title, info))
        .build();
  }

  private Box createInfoBox(String description) {
    final Box info = Box
        .builder()
        .layout(FlexLayout.BASELINE)
        .spacing(FlexMarginSize.MD)
        .contents(asList(
            Text.builder()
                .text(description)
                .color("#aaaaaa")
                .size(FlexFontSize.SM)
                .build()
        ))
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .margin(FlexMarginSize.LG)
        .spacing(FlexMarginSize.SM)
        .contents(asList(info))
        .build();
  }

}