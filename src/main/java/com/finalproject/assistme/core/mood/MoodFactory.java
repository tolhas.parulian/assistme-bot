package com.finalproject.assistme.core.mood;

public class MoodFactory {

  /**
   * Produce mood by type.
   * @param type represents mood type
   * @return
   */
  public Mood createMood(String type) {
    Mood mood = null;
    type = type.toLowerCase();
    switch (type) {
      case "happy":
        mood = new HappyMood();
        break;
      case "sad":
        mood = new SadMood();
        break;
      case "tired":
        mood = new TiredMood();
        break;
      case "productive":
        mood = new ProductiveMood();
        break;
      default:
        mood = new OtherMood();
        break;
    }
    
    return mood;
  }
  
}