package com.finalproject.assistme.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "line_user")
public class LineUser implements Serializable {
  protected LineUser() {

  }

  /**
   * LineUser constructor.
   * @param userId represents userId from line app.
   * @param userName represents userName in AssistMe database.
   * @param displayName represents displayName in AssistMe database from line app.
   */
  public LineUser(String userId, String userName, String displayName) {
    this.userId = userId;
    this.userName = userName;
    this.displayName = displayName;
    this.state = "IDLE";
  }

  @Id
  @Column(name = "user_id", unique = true, nullable = false)
  private String userId;

  @Column(name = "user_name", unique = true, nullable = false)
  private String userName;

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "state", nullable = false)
  private String state;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

}
