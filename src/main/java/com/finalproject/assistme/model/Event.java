package com.finalproject.assistme.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "event")
public class Event implements Serializable {

  protected Event() {

  }

  /**
   * Event constructor.
   * @param lineUser lineUser connected to event.
   * @param name represents name of an event.
   * @param category represents category of an event.
   * @param dueDateTime represents dueDate of an event.
   */
  public Event(
      LineUser lineUser,
      String name,
      String category,
      Date dueDateTime) {
    this.lineUser = lineUser;
    this.name = name;
    this.category = category;
    this.createdAt = new Date();
    this.dueDateTime = dueDateTime;
    this.verified = false;
  }

  @Id
  @Column(name = "event_id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long eventId;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(referencedColumnName = "user_id", nullable = false)
  private LineUser lineUser;

  @Column(name = "name")
  private String name;

  @Column(name = "category")
  private String category;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_at", nullable = false,
          updatable = false,
          columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private Date createdAt;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "due_date_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private Date dueDateTime;

  @Column(name = "verified")
  private boolean verified;

  public Long getEventId() {
    return eventId;
  }

  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public LineUser getUser() {
    return lineUser;
  }

  public void setUser(LineUser lineUser) {
    this.lineUser = lineUser;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public Date getDueDateTime() {
    return dueDateTime;
  }

  public void setDueDateTime(Date dueDateTime) {
    this.dueDateTime = dueDateTime;
  }

  public boolean getVerify() {
    return verified;
  }

  public void setVefiry(boolean verify) {
    this.verified = verify;
  }

}
