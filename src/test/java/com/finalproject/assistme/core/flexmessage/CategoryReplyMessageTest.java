package com.finalproject.assistme.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CategoryReplyMessageTest {
    
  @Test
  public void getCategoryReplyMessageTest() {
    CategoryReplyMessage message = new CategoryReplyMessage();
    assertEquals("Choose category for the event\n Or others with '/choose [category name]'",
            message.getCategoryReplyMessage().getText());
  }
}