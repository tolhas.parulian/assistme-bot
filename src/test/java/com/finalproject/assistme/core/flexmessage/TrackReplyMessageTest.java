package com.finalproject.assistme.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.wrapper.spotify.model_objects.specification.Track;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

public class TrackReplyMessageTest {

  @Test
  public void testGetMessageMethod() {
    TrackReplyMessage message = new TrackReplyMessage();
    assertEquals("assistme Bot sent you list of tracks. Please choose one of them:",
        message.get(new ArrayList<Track>()).getText());

  }
}
