package com.finalproject.assistme.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PlaylistButtonTest {

  @Test
  public void testGetMessageMethod() {
    PlaylistButton message = new PlaylistButton();
    try {
      assertEquals("assistme-Bot sent you a playlist button", message.get("sad").getAltText());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
