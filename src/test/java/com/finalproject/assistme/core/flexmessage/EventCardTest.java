package com.finalproject.assistme.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import java.util.Date;
import org.junit.jupiter.api.Test;


public class EventCardTest {

  @Test
  public void testGetMessageMethod() {
    EventCard eventCard = new EventCard();
    LineUser lineUser = new LineUser("1", "test", "test");
    Event event = new Event(lineUser, "bobo", "bobo", new Date());
    assertEquals("AssistMe-Bot sent you an event card", eventCard.get(event, "test").getAltText());
  }
}
