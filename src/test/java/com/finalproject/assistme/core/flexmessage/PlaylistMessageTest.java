package com.finalproject.assistme.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PlaylistMessageTest {

  @Test
  public void testGetMessageMethod() {
    PlaylistMessage message = new PlaylistMessage();
    assertEquals("assistme-Bot sent you a playlist", message.get("https://example.com",
        "https://open.spotify.com", "test", "test", "/test").getAltText());
  }
}
