package com.finalproject.assistme.core.present;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ImageTest {

  private Image image;

  @BeforeEach
  public void setUp() {
    image = new Image("dummy");
  }

  @Test
  public void testGetImageMethod() {
    try {
      assertEquals("dummy", image.getImage());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}