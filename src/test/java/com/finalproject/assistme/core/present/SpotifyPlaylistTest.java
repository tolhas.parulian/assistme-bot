package com.finalproject.assistme.core.present;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import com.finalproject.assistme.musicplayer.Spotify;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class SpotifyPlaylistTest {

  private SpotifyPlaylist spotifyPlaylist;

  @BeforeEach
  public void setUp() {
    Spotify.clientCredentials_Sync();
    spotifyPlaylist = new SpotifyPlaylist("dummy");
  }

  @Test
  public void getSpotifyPlaylistTest() {
    assertEquals("dummy", spotifyPlaylist.getSpotifyPlaylist());
  }

  @Test
  public void getPlaylistsTest() {
    assertNotEquals(null, spotifyPlaylist.getPlaylists("happy"));
  }

  @Test
  public void getTracksTest() {
    assertEquals(null, spotifyPlaylist.getTracks("3dV99CACGvTGr8qkm9leud"));
  }
}