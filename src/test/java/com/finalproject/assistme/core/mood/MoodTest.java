package com.finalproject.assistme.core.mood;

import com.finalproject.assistme.core.present.Present;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class MoodTest {
    
  Mood mood;

  @BeforeEach
  public void setUp() {
    mood = Mockito.mock(Mood.class);
  }

  @Test
  public void testMethodProcess() {
    Mockito.doCallRealMethod().when(mood).process();
    Present present = mood.process();
    Mockito.doReturn(present).when(mood).process();
  }
}
