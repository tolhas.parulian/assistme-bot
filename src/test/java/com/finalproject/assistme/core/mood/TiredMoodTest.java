package com.finalproject.assistme.core.mood;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.ImageUrlList;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.QuoteList;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TiredMoodTest {

  private Mood mood;
  private ImageUrlList imageUrlList;
  private QuoteList quoteList;

  @BeforeEach
  public void setUp() {
    mood = new TiredMood();
    imageUrlList = new ImageUrlList();
    quoteList = new QuoteList();
  }

  @Test
  public void testMethodGetType() {
    assertEquals("Tired",mood.getType());
  }

  @Test
  public void testMethodGetImage() {
    Image image = mood.getImage();
    try {
      assertTrue(Arrays.asList(imageUrlList.tiredList).contains(image.getImage()));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testMethodGetQuote() {
    Quote quote = mood.getQuote();
    try {
      assertTrue(Arrays.asList(quoteList.tiredList).contains(quote.getQuote()));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testMethodGetSpotifyPlaylist() {
    SpotifyPlaylist spotifyPlaylist = mood.getSpotifyPlaylist();
    assertEquals("Tired spotify playlist",spotifyPlaylist.getSpotifyPlaylist());
  }
}
