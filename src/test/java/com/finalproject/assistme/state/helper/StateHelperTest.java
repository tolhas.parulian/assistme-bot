package com.finalproject.assistme.state.helper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.IdleState;
import com.finalproject.assistme.state.InputCategoryState;
import com.finalproject.assistme.state.InputEventNameState;
import com.finalproject.assistme.state.InputEventReminderState;
import com.finalproject.assistme.state.InputEventTimeState;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StateHelperTest {
  @Spy
  @InjectMocks
  StateHelper stateHelper;

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  IdleState idleState;

  @Test
  public void contextLoads() throws Exception {
    assertThat(stateHelper).isNotNull();
  }

  @Test
  public void allTest() {
    when(lineUserRepository.isLineUserRegistered("1")).thenReturn(true);
    when(lineUserRepository.isLineUserRegistered("2")).thenReturn(false);
    when(lineUserRepository.findStateByUserId("1")).thenReturn(IdleState.DB_COL_NAME);
  
    stateHelper.toState("random");
    stateHelper.toState(InputCategoryState.DB_COL_NAME);
    stateHelper.toState(InputEventNameState.DB_COL_NAME);
    stateHelper.toState(InputEventReminderState.DB_COL_NAME);
    stateHelper.toState(InputEventTimeState.DB_COL_NAME);

    assertEquals(stateHelper.getUserState("1"), idleState);
    assertEquals(stateHelper.getUserState("2"), idleState);

  }
}