package com.finalproject.assistme.state;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IdleStateTest {

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  EventRepository eventRepository;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture2;

  @Spy
  @InjectMocks
  private IdleState stateTest;

  @Test
  public void contextLoads() throws Exception {
    assertThat(stateTest).isNotNull();
  }

  @Before
  public void setUp() throws ExecutionException, InterruptedException {
    LineUser userTest = new LineUser("1", "test", "test");
    userTest.setState(InputCategoryState.DB_COL_NAME);
    Event eventTest = new Event(userTest, "test", "test", new Date());
    ArrayList<Event> listEventTest = new ArrayList<>();
    listEventTest.add(eventTest);

    lineUserRepository.save(userTest);
    eventRepository.save(eventTest);

    when(eventRepository.findUnverifiedEventByUserId(anyString())).thenReturn(eventTest);
    when(eventRepository.findEventByEventId(1l,"1")).thenReturn(eventTest);
    when(eventRepository.findEventByEventId(2l,"1")).thenReturn(null);
    when(eventRepository.findEventsByCategory("general", "1")).thenReturn(listEventTest);
    when(eventRepository.findEventsByCategory("general", "2")).thenReturn(new ArrayList<>());
    when(eventRepository.findEventsByUserIdAsync("1")).thenReturn(eventCompletableFuture);
    when(eventRepository.findEventsByUserIdAsync("1").get()).thenReturn(listEventTest);
    when(eventRepository.findEventsByUserIdAsync("2")).thenReturn(eventCompletableFuture2);
    when(eventRepository.findEventsByUserIdAsync("2").get()).thenReturn(new ArrayList<>());
  }

  @Test
  public void chooseCategoryTest() {
    assertEquals(stateTest.chooseCategory("test", "1"), "IDLE");
  }

  @Test
  public void inputCategoryTest() {
    assertEquals(stateTest.inputCategory("blabla", "1"), "IDLE");
  }

  @Test
  public void inputEventNameTest() {
    assertEquals(stateTest.inputEventName("test", "1"), "SUCCESS");
  }

  @Test
  public void inputEventTimeTest() {
    assertEquals(stateTest.inputEventTime(new Date(), "1"), "IDLE");
  }

  @Test
  public void undoInputEventTest() {
    assertEquals(stateTest.undoInputEvent("1"), "IDLE");
  }

  @Test
  public void deleteEventTest() {
    assertEquals(stateTest.deleteEvent("1", "1"), "SUCCESS");
  }

  @Test
  public void toStringTest() {
    assertEquals("IdleState", stateTest.toString());
  }

  @Test
  public void getEventTest() {
    List<Message> response = stateTest.getEvent("1", "1", "test");
    Mockito.verify(stateTest).getEvent("1", "1", "test");
    assertTrue(response.size() > 0);
  }

  @Test
  public void getEventNullTest() {
    List<Message> response = stateTest.getEvent("2", "1", "test");
    Mockito.verify(stateTest).getEvent("2", "1", "test");
    assertTrue(response.size() > 0);
  }

  @Test
  public void getAllEventsTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.getAllEvents("1", "test");
    Mockito.verify(stateTest).getAllEvents("1", "test");
    CompletableFuture<List<Event>> lineUserAsync = eventRepository.findEventsByUserIdAsync("1");
    assertTrue(response.get(0) instanceof FlexMessage);
  }

  @Test
  public void getAllEventsEmptyTest() {
    List<Message> response = stateTest.getAllEvents("2", "test");
    Mockito.verify(stateTest).getAllEvents("2", "test");
    assertTrue(response.get(0).equals(Messages.EVENT_NOT_FOUND));
  }

  @Test
  public void getEventsByCategoryTest() {
    List<Message> response = stateTest.getEventsByCategory("general", "1",  "test");
    Mockito.verify(stateTest).getEventsByCategory("general", "1",  "test");
    assertTrue(response.size() > 0);
  }

  @Test
  public void getEventsByCategoryEmptyTest() {
    List<Message> response = stateTest.getEventsByCategory("general", "2",  "test");
    Mockito.verify(stateTest).getEventsByCategory("general", "2",  "test");
    assertTrue(response.size() > 0);
    assertTrue(response.get(0).equals(Messages.NO_EVENT_WITH_PROVIDED_CATEGORY));
  }

}