package com.finalproject.assistme.state;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InputCategoryStateTest {

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  EventRepository eventRepository;

  @Spy
  @InjectMocks
  private InputCategoryState stateTest;

  @Test
  public void contextLoads() throws Exception {
    assertThat(stateTest).isNotNull();
  }

  @Before
  public void setUp() {
    LineUser userTest = new LineUser("1", "test", "test");
    userTest.setState(InputCategoryState.DB_COL_NAME);
    Event eventTest = new Event(userTest, "test", "test", new Date());

    lineUserRepository.save(userTest);
    eventRepository.save(eventTest);

    when(lineUserRepository.findLineUserByUserId(anyString())).thenReturn(userTest);
    when(eventRepository.findUnverifiedEventByUserId(anyString())).thenReturn(eventTest);
  }

  @Test
  public void chooseCategoryTest() {
    assertEquals(stateTest.chooseCategory("1", "1"), "SUCCESS");
    assertEquals(stateTest.chooseCategory("2", "1"), "SUCCESS");
    assertEquals(stateTest.chooseCategory("3", "1"), "SUCCESS");
    assertEquals(stateTest.chooseCategory("blabla", "1"), "IDLE");
  }

  @Test
  public void inputCategoryTest() {
    assertEquals(stateTest.inputCategory("blabla", "1"), "SUCCESS");
  }

  @Test
  public void inputEventNameTest() {
    assertEquals(stateTest.inputEventName("test", "1"), "INPUT_CATEGORY");
  }

  @Test
  public void inputEventTimeTest() {
    assertEquals(stateTest.inputEventTime(new Date(), "1"), "INPUT_CATEGORY");
  }

  @Test
  public void undoInputEventTest() {
    assertEquals(stateTest.undoInputEvent("1"), "UNDO");
  }

  @Test
  public void deleteEventTest() {
    assertEquals(stateTest.deleteEvent("1", "1"), "INPUT_CATEGORY");
  }

  @Test
  public void toStringTest() {
    assertEquals("InputCategoryState", stateTest.toString());
  }

  @Test
  public void reportMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    assertEquals(stateTest.reportMood(), response);
  }

  @Test
  public void processMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    assertEquals(stateTest.processMood("happy"), response);
  }

  @Test
  public void othersTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.OTHERS_TEXT);
    assertEquals(stateTest.others(), response);
  }

  @Test
  public void helpTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    assertEquals(stateTest.help(), response);
  }

  @Test
  public void getEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    assertEquals(stateTest.getEvent("1", "1", "test"), response);
  }

  @Test
  public void getAllEventsTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    assertEquals(stateTest.getAllEvents("1", "test"), response);
  }

  @Test
  public void getEventsByCategoryTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.INVALID_COMMAND_AT_WRONG_STATE);
    assertEquals(stateTest.getEventsByCategory("test", "1", "test"), response);
  }

}