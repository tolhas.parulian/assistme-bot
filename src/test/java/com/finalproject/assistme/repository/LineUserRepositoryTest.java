package com.finalproject.assistme.repository;

import com.finalproject.assistme.model.LineUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LineUserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LineUserRepository lineUserRepository;

    @Mock
    private LineUserRepository lineUserRepositoryMock;

    @Test
    public void testFindLineUserByUserNameThenReturLineUser() {
        LineUser lineUser = new LineUser("1", "agus", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        LineUser lineUserFound = lineUserRepository.findLineUserByUserName(lineUser.getUserName());
        assertThat(lineUser.getUserName()).isEqualTo(lineUserFound.getUserName());
    }

    @Test
    public void testFindLineUserByUserIdThenReturnLineUser() {
        LineUser lineUser = new LineUser("2", "brem", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        LineUser lineUserFound = lineUserRepository.findLineUserByUserId(lineUser.getUserId());
        assertThat(lineUserFound.getUserId()).isEqualTo(lineUser.getUserId());
    }

    @Test
    public void testFindLineUserByUserIdAsyncThenReturnLineUser() {
        LineUser lineUser = new LineUser("2", "brem", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        CompletableFuture<LineUser> lineUserFound = this.lineUserRepositoryMock.findLineUserByUserIdAsync(lineUser.getUserId());
        verify(lineUserRepositoryMock, times(1)).findLineUserByUserIdAsync(lineUser.getUserId());
    }

    @Test
    public void testIsLineUserRegisteredThenReturnBoolean() {
        LineUser lineUser = new LineUser("3", "vio", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        boolean isUserRegistered = lineUserRepository.isLineUserRegistered(lineUser.getUserId());
        assertTrue(isUserRegistered);
    }

    @Test
    public void testIsUserNameUniqueThenReturnBoolean() {
        LineUser lineUser = new LineUser("4", "voma", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        boolean isUserRegistered = lineUserRepository.isUserNameUnique(lineUser.getUserName());
        assertTrue(isUserRegistered);
    }

    @Test
    public void testFindStateByUserIdThenReturnString() {
        LineUser lineUser = new LineUser("5", "gona", "kuwuk");
        entityManager.persist(lineUser);
        entityManager.flush();
        String state = lineUserRepository.findStateByUserId(lineUser.getUserId());
        assertThat(state).isEqualTo("IDLE");
    }


}
