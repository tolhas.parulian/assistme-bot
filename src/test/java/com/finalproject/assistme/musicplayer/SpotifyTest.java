package com.finalproject.assistme.musicplayer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class SpotifyTest {

  @Test
  public void getTrackTest() {
    Spotify.clientCredentials_Sync();
    assertEquals(
        "https://p.scdn.co/mp3-preview/d638758d65ddc7c8d15c376c1534f3e5a8afd63e?cid=28145851ec444ef088584a4233382680",
        Spotify.getTrackSync("50BlgWI61TOVUE1Pc37yKm"));
  }

  @Test
  public void getTrackTestException() {
    Spotify.clientCredentials_Sync();
    assertEquals("Error: invalid id", Spotify.getTrackSync("zzzzzzzzzzzzzzzz"));
  }

  @Test
  public void getPlaylistTrackSizeTest() {
    Spotify.clientCredentials_Sync();
    assertNotEquals(5, Spotify.getPlaylistsTracksSync("3dV99CACGvTGr8qkm9leud"));
  }

  @Test
  public void getPlaylistTrackExceptionTest() {
    Spotify.clientCredentials_Sync();
    assertEquals(null, Spotify.getPlaylistsTracksSync("zzzzzzzzzzzzzzzz"));
  }

  @Test
  public void searchPlaylistTest() {
    Spotify.clientCredentials_Sync();
    assertNotEquals(null, Spotify.searchPlayList("Final Fantasy"));
  }


}


