package com.finalproject.assistme;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;

public class EventTestUtil {

	private EventTestUtil() {

	}

	public static MessageEvent<TextMessageContent> createDummyTextMessage(String text,
        String userId) {
        return new MessageEvent<>("replyToken", new UserSource(userId),
            new TextMessageContent("id", text),
            Instant.parse("2020-01-01T00:00:00.000Z"));
    }
}