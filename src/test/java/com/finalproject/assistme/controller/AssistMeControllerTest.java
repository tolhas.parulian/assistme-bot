package com.finalproject.assistme.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.finalproject.assistme.EventTestUtil;
import  com.finalproject.assistme.handler.ApplicationService;
import com.finalproject.assistme.handler.UniversalHandler;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.IdleState;
import com.finalproject.assistme.state.InputEventNameState;
import com.finalproject.assistme.state.InputEventReminderState;
import com.finalproject.assistme.state.State;
import com.finalproject.assistme.state.helper.StateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.linecorp.bot.model.event.source.UserSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class AssistMeControllerTest {

  @Spy
  @InjectMocks
  private AssistMeController assistMeController;

  @Mock
  private LineMessagingClient lineMessagingClient;

  @Mock
  private CompletableFuture<UserProfileResponse> usrprcompletableFuture;
  
  @Mock
  private CompletableFuture<BotApiResponse> botCompletableFuture;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture;

  @Mock
  private LineUserRepository lineUserRepository;

  @Mock
  private EventRepository eventRepository;

  @Mock
  private StateHelper stateHelper;

  // @Spy
  // private InputEventNameState state;

  @Mock
  private State state;

  @Mock
  private UniversalHandler universalHandler;

  @Mock
  private ApplicationService applicationService;

  @Test
  public void contextLoads() throws Exception {
    assertThat(assistMeController).isNotNull();
  }

  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  @Before
  public void setUp() throws ExecutionException, InterruptedException, ParseException {
    Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
    List<Event> listEvent = new ArrayList<>();
    LineUser lineUser = new LineUser("1", "lah", "dummy");
    LineUser lineUser3 = new LineUser("3", "kelar", "dummy3");
    LineUser lineUser4 = new LineUser("4", "kelar4", "dummy4");
    lineUser3.setState(InputEventReminderState.DB_COL_NAME);
    lineUser4.setState(InputEventNameState.DB_COL_NAME);
    Event event = new Event(
            (lineUserRepository.findLineUserByUserId("1")),
            "waw",
            "work",
            dueDateTime);
    event.setVefiry(false);
    listEvent.add(event);
    UserProfileResponse userProfileResponse = new UserProfileResponse(
        "dummy", "1", "file://stub", "hello");
    UserProfileResponse userProfileResponse2 = new UserProfileResponse(
        "dummy", "2", "file://stub", "hello");
    //user '1' for registered user
    when(lineMessagingClient.getProfile("1")).thenReturn(usrprcompletableFuture);
    when(lineMessagingClient.getProfile("1").get()).thenReturn(userProfileResponse);
    //user '2' for unregistered user
    when(lineMessagingClient.getProfile("2")).thenReturn(usrprcompletableFuture);
    when(lineMessagingClient.getProfile("2").get()).thenReturn(userProfileResponse);
    when(lineMessagingClient.getProfile("5")).thenReturn(usrprcompletableFuture);
    when(lineMessagingClient.getProfile("5").get()).thenReturn(userProfileResponse);
    when(lineUserRepository.isLineUserRegistered("2")).thenReturn(false);
    when(lineUserRepository.isLineUserRegistered("1")).thenReturn(true);
    when(lineUserRepository.isLineUserRegistered("3")).thenReturn(true);
    when(lineUserRepository.isLineUserRegistered("4")).thenReturn(true);
    when(lineUserRepository.isLineUserRegistered("5")).thenReturn(true);
    when(lineUserRepository.findLineUserByUserId("1")).thenReturn(lineUser);
    when(lineUserRepository.findLineUserByUserId("3")).thenReturn(lineUser3);
    when(lineUserRepository.findLineUserByUserId("4")).thenReturn(lineUser4);
    when(stateHelper.getUserState("1")).thenReturn(new IdleState());
    when(stateHelper.getUserState("3")).thenReturn(new InputEventReminderState());
    when(eventRepository.findUnverifiedEventByUserId("5")).thenReturn(event);

    }

  @Test
  public void getMessageTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("halo!","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getAudioMessageTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
            new TextMessageContent("id","/music music"), Instant.parse("2020-01-01T00:00:00.000Z"));
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getUserDisplayNameTest() throws ExecutionException, InterruptedException {
    assertEquals(assistMeController.getUserDisplayName("1"), "dummy");
  }

  @Test
  public void registerLineUserPassedTest() throws ExecutionException, InterruptedException {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("2");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/register landi");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }
  
  @Test
  public void registerLineUserNotPassedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("2"),
            new TextMessageContent("id", "/register"), Instant.parse("2020-01-01T00:00:00.000Z"));
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void lineUserNotRegisteredTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken",
            new UserSource("2"), new TextMessageContent("id", "/apasaja"),
            Instant.parse("2020-01-01T00:00:00.000Z"));
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void createWrongEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/createEvent");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood happy","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void testCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
        new TextMessageContent("id", "/playlist happy"), Instant.parse("2020-01-01T00:00:00.000Z"));

    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void showTracksCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
        new TextMessageContent("id", "/showTracks 37i9dQZF1DXdPec7aLTmlC"),
        Instant.parse("2020-01-01T00:00:00.000Z"));

    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void createEventNotPassedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("5");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/createEvent uia");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void createEventPassedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("3");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/createEvent makan");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void chooseCategoryFailedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/choose");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void chooseCategoryChooseTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/choose 1");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void chooseCategoryInputTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/choose kentang");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void dateTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/date 2020-20-20");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void dateFailedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/date");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void dateWrongTimeTest() throws Exception, ParseException {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("3");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/date 2020-01-01");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void dateWrongFormatTest() throws Exception, ParseException {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("3");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/date 47832749");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void undoInputNothingTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/undoInput");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void undoInputTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("4");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/undoInput");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void deleteEventFailedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/deleteEvent");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void deleteEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("3");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/deleteEvent 1");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventFailedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/getEvent");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/getEvent 1");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getAllEventsTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/getAllEvents");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventsByCategoryTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent(
            "id", "/getEventsbyCategory lifestyle");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventsByCategoryNotPassedTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/getEventsbyCategory");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventsByCategoryNotFoundTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent(
            "id", "/getEventsbyCategory batu");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void testGetUserState() throws Exception {
    assistMeController.isLineUserRegistered("2");
    assistMeController.isLineUserRegistered("1");
    assertEquals(assistMeController.isLineUserRegistered("1"), true);
    assertEquals(assistMeController.isLineUserRegistered("2"), false);
  }

  @Test
  public void reportCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/reportMood","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventNameTest() {
    String[] messageList = new String[] {"makan", "nasi"};
    String message = assistMeController.getEventName(messageList);
    assertEquals("makan nasi", message);
  }

  @Test
  public void subArray() {
    String[] messageListWithCommand = new String[] {"createEvent/", "makan", "nasi"};
    String[] messageList = assistMeController.subArray(messageListWithCommand, 1, 2);
    assertArrayEquals(new String[] {"makan"}, messageList);
  }

  @Test
  public void testFlexCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/testFlex","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void testFlexListCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/testFlexList","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void testHelpCommand() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/help","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

}