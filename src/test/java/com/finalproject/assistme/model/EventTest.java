package com.finalproject.assistme.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EventTest {

  private Event event;
    
  private LineUser lineUser;

  private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  @BeforeEach
  public void setUp() throws ParseException {
    Date dueDateTime = this.format.parse("2030-02-10 01:14:15.000");
    this.lineUser = new LineUser("1", "agus", "kuwuk");
    this.event = new Event(lineUser, "ngerjain pr", "tugas", dueDateTime);
    this.event.setVefiry(true);
  }

  @Test
  public void testSetGetEventId() {
    assertEquals(this.event.getEventId(), this.event.getEventId());
    this.event.setEventId(10L);
    assertEquals(10L, (long) this.event.getEventId());
  }

  @Test
  public void testSetGetUser() {
    assertEquals(lineUser, this.event.getUser());
    LineUser newUser = new LineUser("2", "andi", "kuwuk");
    this.event.setUser(newUser);
    assertEquals(newUser, this.event.getUser());
  }

  @Test
  public void testSetgetName() {
    assertEquals("ngerjain pr", this.event.getName());
    this.event.setName("ngerjain tugas");
    assertEquals("ngerjain tugas", this.event.getName());
  }

  @Test
  public void testSetGetCategory() {
    assertEquals("tugas", this.event.getCategory());
    this.event.setCategory("aktivitas");
    assertEquals("aktivitas", this.event.getCategory());
  }


  @Test
  public void testGetCreatedAt() throws ParseException {
    Date createdAt = format.parse("2010-01-10 01:14:15.000");
    assertNotEquals(createdAt, event.getCreatedAt());
  }

  @Test
  public void testSetGetDueDateTime() throws ParseException {
    Date dueDateTime = this.format.parse("2030-02-10 01:14:15.000");
    Date dueDateTimeAlter = this.format.parse("2030-06-10 01:14:15.000");
    assertEquals(dueDateTime, this.event.getDueDateTime());
    this.event.setDueDateTime(dueDateTimeAlter);
    assertEquals(dueDateTimeAlter, this.event.getDueDateTime());
  }

  @Test
  public void testSetGetVerify() {
    assertEquals(true, this.event.getVerify());
    this.event.setVefiry(false);
    assertEquals(false, this.event.getVerify());
  }
}
