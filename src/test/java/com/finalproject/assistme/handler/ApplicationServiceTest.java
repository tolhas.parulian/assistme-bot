package com.finalproject.assistme.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ApplicationServiceTest {

	private ApplicationService applicationService;
	private Handler handler;

	@BeforeEach
	public void setUp() {
		handler = new UniversalHandler();
		applicationService = new ApplicationService(handler);
	}

	@Test
	public void call() {
		applicationService.getResponse();
		applicationService.setHandler(new UniversalHandler());
		assertEquals(true, true);
	}
}