package com.finalproject.assistme.handler.message;

import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MessagesTest {
    @Spy
    @InjectMocks
    Messages messages;

    @Test
    public void contextLoads() throws Exception {
        assertThat(messages).isNotNull();
    }
}